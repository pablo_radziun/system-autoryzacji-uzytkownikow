package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.SiteApplication;
import repositories.DummySiteApplicationRepository;
import repositories.SiteApplicationRepository;

/**
 * Servlet implementation class AddingPremiumServlet
 */
@WebServlet("/adding")
public class AddingPremiumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddingPremiumServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		SiteApplicationRepository repository = new DummySiteApplicationRepository() {
		};
		SiteApplication known = repository.getApplicationByLogin(request.getParameter("login"));
		if(known!=null)
		{
			repository.getApplicationByLogin(request.getParameter("login")).setPremium("yes");
			response.setContentType("text/html");
			response.getWriter().print("Uprawnienia premium zosta�y nadane");
		}
		else
		{
			response.setContentType("text/html");
			response.getWriter().print("B��dny login. Uprawnienia premium nie nadane");
		}
	}

}
