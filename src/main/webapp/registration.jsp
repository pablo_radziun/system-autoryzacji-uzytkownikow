<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Rejestracja</title>
</head>
<body>
<h1>Witamy na naszej stronie :)</h1>
<h4>Wypełnij pola poniżej, aby się zarejestrować:</h4>

<form method ="post" action="registered">
			<div>
				<p>
					Wpisz login &nbsp;&nbsp;&nbsp;
					<input type="text" name="login" required />
				</p>
				<p>
					Wpisz hasło &nbsp;&nbsp;&nbsp;
					<input type="password" id="password" name="password" required />
				</p>
				<p>
					Wpisz ponownie hasło&nbsp;&nbsp;&nbsp;
					<input type="password" id="password2" name="password2" onblur="confirmPass()" required />
				</p>
				<p>
					Wpisz e-mail:&nbsp;&nbsp;&nbsp;
					<input type="email" name="mail" required />
				</p>
				<br/>
				<input type="submit" value="Wyślij"/>
 
			</div>
		</form>
		
		<script type="text/javascript">
    function confirmPass() {
        var pass = document.getElementById("password").value
        var confpass = document.getElementById("password2").value
        if(pass != confpass) {
            alert('Hasło się nie zgadza! Wpisz jeszcze raz!');
        }
    }
</script>

</body>
</html>