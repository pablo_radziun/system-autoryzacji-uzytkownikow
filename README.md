3)	System do zarządzania autoryzacją. 

W systemie istnieją 4 rodzaje użytkowników: 
•	Użytkownik anonimowy: 
o	Brak dostępu do podstron serwisu, 
o	Posiada dostęp do strony rejestrowania nowych użytkowników, 
o	Posiada dostęp do strony logowania 

•	Użytkownik zwykły 
o	Posiada dostęp do swojego profilu 
o	Nie ma potrzeby by miał dostęp do strony rejestrowania, jeśli będzie chciał na nią wejść, ma zostać przekierowany na stroną swojego profilu 
o	Nie ma potrzeby by miał dostęp do strony logowania, jeśli będzie chciał na nią wejść, ma zostać przekierowany na stroną swojego profilu 
o	Brak dostępu do strony nadawania uprawnień „Premium” 

•	Użytkownik z kontem „Premium” 
o	To samo jak zwykły użytkownik 
o	Ma dostęp do strony „premium.jsp” 

•	Administrator 
o	To samo jak użytkownik „Premium” 
o	Ma dostęp do strony gdzie może użytkownikowi dodać lub usunąć uprawnienia „Premium